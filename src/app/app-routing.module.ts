import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainComponent } from './main/main.component';
import { PositionFormComponent } from './position-form/position-form.component';
import { BulkUploadComponent } from './bulk-upload/bulk-upload.component';

export const routes: Routes = [
  { 
    path: '', 
    component: MainComponent,
    children:[
      
      { 
        path: 'positionform', 
        component: PositionFormComponent,
        pathMatch: 'full' 
      },
      
      { 
        path: 'bulkupload', 
        component: BulkUploadComponent 
      },

    ]
  },
  
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
