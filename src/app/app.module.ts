import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { AppRoutingModule } from './/app-routing.module';
import { MainComponent } from './main/main.component';
import { PositionFormComponent } from './position-form/position-form.component';
import { BulkUploadComponent } from './bulk-upload/bulk-upload.component';

import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { CookieService } from 'ngx-cookie-service';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { EdmService } from './edm.service';
import { DragComponent } from './drag/drag.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    PositionFormComponent,
    BulkUploadComponent,
    jqxGridComponent,
    DragComponent

  ],
  imports: [
    BrowserModule,
    NgbModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    
    HttpModule,
    NgbModule.forRoot(),
  ],
  providers: [CookieService, EdmService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
