import { Component, OnInit, ViewChild } from '@angular/core';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

declare var $;

const todoapi = 'http://localhost:3000/Data?uid=';

@Component({
    selector: 'app-bulk-upload',
    templateUrl: './bulk-upload.component.html',
    styleUrls: ['./bulk-upload.component.css']
})
export class BulkUploadComponent implements OnInit {

    @ViewChild('myGrid') myGrid: jqxGridComponent;


    constructor(private httpClient: HttpClient) {
    }

    ngOnInit() {

        console.log(window.screen.width);
        if (window.screen.width == 1600) {
            this.screenwidth = 1276;
        }
        if (window.screen.width == 1440) {
            this.screenwidth = 1145;
        }

        if (window.screen.width == 1280) {
            this.screenwidth = 1008;
        }

        if (window.screen.width > 1310 && window.screen.width <= 1370) {
            this.screenwidth = 1074;
        }
        if (window.screen.width >= 1300 && window.screen.width <= 1310) {
            this.screenwidth = 1025;
        }


    }

    screenwidth: any;
    change: number;
    deletedata: any;
    contact: any[];
    printposition: any;
    datajsonsave = {
        'Output': {
            'error':
                [],
            'position_Id': []

        },

    };

    Rowclick(event: any): void {
        console.log(event);
    }

    source: any =
        {


            datatype: 'xml',
            datafields: [
                { name: 'position_Id', type: 'string' },
                { name: 'effective_Date', type: 'string' },
                { name: 'status', type: 'string' },
                { name: 'legal_Entity', type: 'string' },
                { name: 'bu_Ssu', type: 'string' },
                { name: 'lob_Segment', type: 'string' },
                { name: 'vertical', type: 'string' },
                { name: 'sub_Vertical', type: 'string' },
                { name: 'cost_Center', type: 'string' },
                { name: 'geozone', type: 'string' },
                { name: 'facility', type: 'string' },
                { name: 'employee_Class', type: 'string' },
                { name: 'employee_Type', type: 'string' },
                { name: 'job_Family', type: 'string' },
                { name: 'job_Code', type: 'string' },
                { name: 'internal_Specialization', type: 'string' },
                { name: 'variable_Pay_Plan_Type', type: 'string' },
                { name: 'reporting_Manager', type: 'string' },
                { name: 'bu_Hr_Spoc', type: 'string' }


            ],
            root: 'Products',
            record: 'Product',
            id: 'ProductID',
            url: '../../assets/products.xml'
        };

    dataAdapter: any = new jqx.dataAdapter(this.source);


    /*cellsrenderer = (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
        if (value < 20) {
            return `<span style='margin: 4px; float:${columnproperties.cellsalign}; color: #ff0000;'>${value}</span>`;
        }
        else {
            return `<span style='margin: 4px; float:${columnproperties.cellsalign}; color: grey;'>${value}</span>`;
        }
    };*/

    columngroups: any[] =
        [
            { text: 'Position Section', align: 'center', name: 'PositionSection', id: 'PositionSection' },
            { text: '5 level hirearchy', align: 'center', name: '5levelhirearchy' },
            { text: 'Geozone Facility', align: 'center', name: 'GeozoneFacility' },
            { text: 'Employee class', align: 'center', name: 'Employeeclass' },
            { text: 'Global work', align: 'center', name: 'Globalwork' },
            { text: 'Reporting manager', align: 'center', name: 'Reportingmanager' }
        ];

    columns: any[] =
        [
            { text: 'Position Id', columngroup: 'PositionSection', datafield: 'position_Id', width: 210 },
            { text: 'Effective Date', columngroup: 'PositionSection', datafield: 'effective_Date', width: 210 },
            { text: 'Status', columngroup: 'PositionSection', datafield: 'status', width: 210 },
            { text: 'Legal Entity', columngroup: '5levelhirearchy', datafield: 'legal_Entity', width: 210 },
            { text: 'Business Unit/SSU', columngroup: '5levelhirearchy', datafield: 'bu_Ssu', width: 210 },
            { text: 'LOB/Segment', columngroup: '5levelhirearchy', datafield: 'lob_Segment', width: 210 },
            { text: 'Vertical', columngroup: '5levelhirearchy', datafield: 'vertical', width: 210 },
            { text: 'Sub Vertical', columngroup: '5levelhirearchy', datafield: 'sub_Vertical', width: 210 },
            { text: 'Cost Center', columngroup: '5levelhirearchy', datafield: 'cost_Center', width: 210 },
            { text: 'Geozone', columngroup: 'GeozoneFacility', datafield: 'geozone', width: 210 },
            { text: 'Facility', columngroup: 'GeozoneFacility', datafield: 'facility', width: 210 },
            { text: 'Employee Class', columngroup: 'Employeeclass', datafield: 'employee_Class', width: 210 },
            { text: 'Employee Type', columngroup: 'Employeeclass', datafield: 'employee_Type', width: 210 },
            { text: 'Job Family', columngroup: 'Globalwork', datafield: 'job_Family', width: 210 },
            { text: 'Job Code', columngroup: 'Globalwork', datafield: 'job_Code', width: 210 },
            { text: 'Internal Specialization', columngroup: 'Globalwork', datafield: 'internal_Specialization', width: 210 },
            { text: 'Variable Pay Plan Type', columngroup: 'Globalwork', datafield: 'variable_Pay_Plan_Type', width: 210 },
            { text: 'Reporting Manager', columngroup: 'Reportingmanager', datafield: 'reporting_Manager', width: 210 },
            { text: 'BU HR  SPOC', columngroup: 'Reportingmanager', datafield: 'bu_Hr_Spoc', width: 210 },

        ];


    datasave() {
        console.log(this.contact);
        // this.dataarray = XLSX.utils.sheet_to_json(this.datajsonsave,{raw:true})
        //console.log(this.dataarray)
    }

    final(aaa) {
        if (aaa) {
            console.log(aaa);

        }
        else {
            $('#showdiv').css('display', 'none');
            $('#success-btn').click();
        }
    }

    expotdataofexcel() {
        let value = this.myGrid.exportdata('json');

        let value1 = this.myGrid.savestate();
        let value6 = this.myGrid.getrowid(0);
        let value2 = this.myGrid.getrows();
        let value3 = this.myGrid.updating();

        let value4 = this.myGrid.getcelltext(0, 'position_Id');

        console.log(value2.length);
        /**code made by deepak */
        let totalRows = this.myGrid.getrows();
        $('.jqx-grid-validation').css('display', 'none');
        $('.jqx-grid-validation-arrow-up').css('display', 'none');
        for (let i = 0; i < totalRows.length; i++) {
            let a = totalRows[i];
            console.log(a)
            if (a.position_Id == '') {
                this.myGrid.showvalidationpopup(i, 'position_Id', 'Enter valid Position Id');
            }
            if (a.effective_Date == '') {
                this.myGrid.showvalidationpopup(i, 'effective_Date', 'Enter valid date');
            }
            if (a.status == '') {
                this.myGrid.showvalidationpopup(i, 'status', 'Enter valid stats');
            }
            if (a.legal_Entity=='') {
                this.myGrid.showvalidationpopup(i, 'legal_Entity', 'Enter valid  Legal Entity');
            }
            if (a.bu_Ssu=='') {
                this.myGrid.showvalidationpopup(i, 'bu_Ssu', 'Enter valid  Bu Ssu');
            }
            if (a.lob_Segment=='') {
                this.myGrid.showvalidationpopup(i, 'lob_Segment', 'Enter valid  Lob Segment');
            }
            if (a.vertical=='') {
                this.myGrid.showvalidationpopup(i, 'vertical', 'Enter valid  vertical');
            }
            if (a.cost_Center=='') {
                this.myGrid.showvalidationpopup(i, 'cost_Center', 'Enter valid  Cost Center');
            }
            if (a.geozone=='') {
                this.myGrid.showvalidationpopup(i, 'geozone', 'Enter valid  geozone');
            }
            if (a.facility=='') {
                this.myGrid.showvalidationpopup(i, 'facility', 'Enter valid  facility');
            }
            if (a.employee_Class=='') {
                this.myGrid.showvalidationpopup(i, 'employee_Class', 'Enter valid  Employee Class');
            }
            if (a.employee_Type=='') {
                this.myGrid.showvalidationpopup(i, 'employee_Type', 'Enter valid  Employee Type'); 
            }
            if (a.job_Family=='') {
                this.myGrid.showvalidationpopup(i, 'job_Family', 'Enter valid  Job Family');
            }
            if (a.job_Code=='') {
                this.myGrid.showvalidationpopup(i, 'job_Code', 'Enter valid  Job Code');
            }
            if (a.variable_Pay_Plan_Type=='') {
                this.myGrid.showvalidationpopup(i, 'variable_Pay_Plan_Type', 'Enter valid  Variable Pay Plan Type');
            }
            if (a.reporting_Manager=='') { 
                this.myGrid.showvalidationpopup(i, 'reporting_Manager', 'Enter valid  Reporting Manager')
            }
            if (a.bu_Hr_Spoc=='') { 
                this.myGrid.showvalidationpopup(i, 'bu_Hr_Spoc', 'Enter valid  BU HR SPOC'); 
            }

        }
        /*
        
                var i;
                var value_final = {};
                for (i = 0; i < value2.length; i++) {
        
        
                    if (this.myGrid.getcelltext(i, 'position_Id') == '' || this.myGrid.getcelltext(i, 'effective_Date') == '' || this.myGrid.getcelltext(i, 'status') == '' || this.myGrid.getcelltext(i, 'legal_Entity') == '' || this.myGrid.getcelltext(i, 'bu_Ssu') == '' || this.myGrid.getcelltext(i, 'lob_Segment') == '' || this.myGrid.getcelltext(i, 'vertical') == '' || this.myGrid.getcelltext(i, 'cost_Center') == '' || this.myGrid.getcelltext(i, 'geozone') == '' || this.myGrid.getcelltext(i, 'facility') == '' || this.myGrid.getcelltext(i, 'employee_Class') == '' || this.myGrid.getcelltext(i, 'employee_Type') == '' || this.myGrid.getcelltext(i, 'job_Family') == '' || this.myGrid.getcelltext(i, 'job_Code') == '' || this.myGrid.getcelltext(i, 'variable_Pay_Plan_Type') == '' || this.myGrid.getcelltext(i, 'reporting_Manager') == '' || this.myGrid.getcelltext(i, 'bu_Hr_Spoc') == '') {
        
                        console.log(this.myGrid.getcelltext(i, 'position_Id'))
                        if (this.myGrid.getcelltext(i, 'position_Id') == '') {
                            this.myGrid.showvalidationpopup(i, 'position_Id', 'Enter valid Position Id');
                        }
                        else {
                            // $('.jqx-grid-validation').css('display', 'none');
                            // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                        }
                        if (this.myGrid.getcelltext(i, 'effective_Date') == '') {
                            this.myGrid.showvalidationpopup(i, 'effective_Date', 'Enter valid  status');
                        }
                        else {
        
                            // $('.jqx-grid-validation').css('display', 'none');
                            // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                        }
                        if (this.myGrid.getcelltext(i, 'status') == '') {
                            this.myGrid.showvalidationpopup(i, 'status', 'Enter valid  status');
                        }
                        else {
        
                            // $('.jqx-grid-validation').css('display', 'none');
                            // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                        }
                        if (this.myGrid.getcelltext(i, 'legal_Entity') == '') {
                            this.myGrid.showvalidationpopup(i, 'legal_Entity', 'Enter valid  Legal Entity');
                        }
                        else {
        
                            // $('.jqx-grid-validation').css('display', 'none');
                            // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                        }
                        if (this.myGrid.getcelltext(i, 'bu_Ssu') == '') {
                            this.myGrid.showvalidationpopup(i, 'bu_Ssu', 'Enter valid  Bu Ssu');
                        }
                        else {
        
                            // $('.jqx-grid-validation').css('display', 'none');
                            // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                        }
                        if (this.myGrid.getcelltext(i, 'lob_Segment') == '') {
                            this.myGrid.showvalidationpopup(i, 'lob_Segment', 'Enter valid  Lob Segment');
                        }
                        else {
        
                            // $('.jqx-grid-validation').css('display', 'none');
                            // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                        }
        
                        if (this.myGrid.getcelltext(i, 'vertical') == '') {
                            this.myGrid.showvalidationpopup(i, 'vertical', 'Enter valid  vertical');
                        }
                        else {
        
                            // $('.jqx-grid-validation').css('display', 'none');
                            // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                        }
        
        
                        if (this.myGrid.getcelltext(i, 'cost_Center') == '') {
                            this.myGrid.showvalidationpopup(i, 'cost_Center', 'Enter valid  Cost Center');
                        }
                        else {
        
                            // $('.jqx-grid-validation').css('display', 'none');
                            // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                        }
        
                        if (this.myGrid.getcelltext(i, 'geozone') == '') {
                            this.myGrid.showvalidationpopup(i, 'geozone', 'Enter valid  geozone');
                        }
                        else {
        
                            // $('.jqx-grid-validation').css('display', 'none');
                            // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                        }
        
                        if (this.myGrid.getcelltext(i, 'facility') == '') {
                            this.myGrid.showvalidationpopup(i, 'facility', 'Enter valid  facility');
                        }
                        else {
        
                            // $('.jqx-grid-validation').css('display', 'none');
                            // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                        }
        
        
                        if (this.myGrid.getcelltext(i, 'employee_Class') == '') {
                            this.myGrid.showvalidationpopup(i, 'employee_Class', 'Enter valid  Employee Class');
                        }
                        else {
        
                            // $('.jqx-grid-validation').css('display', 'none');
                            // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                        }
        
                        if (this.myGrid.getcelltext(i, 'employee_Type') == '') {
                            this.myGrid.showvalidationpopup(i, 'employee_Type', 'Enter valid  Employee Type');
                        }
                        else {
        
                            // $('.jqx-grid-validation').css('display', 'none');
                            // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                        }
        
                        if (this.myGrid.getcelltext(i, 'job_Family') == '') {
                            this.myGrid.showvalidationpopup(i, 'job_Family', 'Enter valid  Job Family');
                        }
                        else {
        
                            // $('.jqx-grid-validation').css('display', 'none');
                            // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                        }
        
                        if (this.myGrid.getcelltext(i, 'job_Code') == '') {
                            this.myGrid.showvalidationpopup(i, 'job_Code', 'Enter valid  Job Code');
                        }
                        else {
        
                            // $('.jqx-grid-validation').css('display', 'none');
                            // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                        }
        
                        if (this.myGrid.getcelltext(i, 'variable_Pay_Plan_Type') == '') {
                            this.myGrid.showvalidationpopup(i, 'variable_Pay_Plan_Type', 'Enter valid  Variable Pay Plan Type');
                        }
                        else {
        
                            // $('.jqx-grid-validation').css('display', 'none');
                            // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                        }
        
        
                        if (this.myGrid.getcelltext(i, 'reporting_Manager') == '') {
                            this.myGrid.showvalidationpopup(i, 'reporting_Manager', 'Enter valid  Reporting Manager');
                        }
                        else {
        
                            // $('.jqx-grid-validation').css('display', 'none');
                            // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                        }
        
        
                        if (this.myGrid.getcelltext(i, 'bu_Hr_Spoc') == '') {
                            this.myGrid.showvalidationpopup(i, 'bu_Hr_Spoc', 'Enter valid  BU HR SPOC');
                        }
                        else {
        
                            // $('.jqx-grid-validation').css('display', 'none');
                            // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                        }
        
        
                    }
                    else {
                        this.httpClient.post('http://13.90.80.185:3337/user/validate_api/', value2)
                            .map((response: any) => response)
                            .do(data => this.datajsonsave = data)
                            .subscribe(() => {
                                    this.final(this.datajsonsave.Output.error), $('#success-btn').click();
                                }
                            );
                    }
         
         
                    // if (this.myGrid.getcelltext(i, 'position_Id') == '') {
                    //     this.myGrid.showvalidationpopup(i, 'position_Id', 'Enter valid Position Id');
                    // }
                    // else {
                    //     // $('.jqx-grid-validation').css('display', 'none');
                    //     // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                    // }
                    // if (this.myGrid.getcelltext(i, 'effective_Date') == '') {
                    //     this.myGrid.showvalidationpopup(i, 'effective_Date', 'Enter valid  status');
                    // }
                    // else {
        
                    //     // $('.jqx-grid-validation').css('display', 'none');
                    //     // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                    // }
                    // if (this.myGrid.getcelltext(i, 'status') == '') {
                    //     this.myGrid.showvalidationpopup(i, 'status', 'Enter valid  status');
                    // }
                    // else {
        
                    //     // $('.jqx-grid-validation').css('display', 'none');
                    //     // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                    // }
                    // if (this.myGrid.getcelltext(i, 'legal_Entity') == '') {
                    //     this.myGrid.showvalidationpopup(i, 'legal_Entity', 'Enter valid  Legal Entity');
                    // }
                    // else {
        
                    //     // $('.jqx-grid-validation').css('display', 'none');
                    //     // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                    // }
                    // if (this.myGrid.getcelltext(i, 'bu_Ssu') == '') {
                    //     this.myGrid.showvalidationpopup(i, 'bu_Ssu', 'Enter valid  Bu Ssu');
                    // }
                    // else {
        
                    //     // $('.jqx-grid-validation').css('display', 'none');
                    //     // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                    // }
                    // if (this.myGrid.getcelltext(i, 'lob_Segment') == '') {
                    //     this.myGrid.showvalidationpopup(i, 'lob_Segment', 'Enter valid  Lob Segment');
                    // }
                    // else {
        
                    //     // $('.jqx-grid-validation').css('display', 'none');
                    //     // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                    // }
        
                    // if (this.myGrid.getcelltext(i, 'vertical') == '') {
                    //     this.myGrid.showvalidationpopup(i, 'vertical', 'Enter valid  vertical');
                    // }
                    // else {
        
                    //     // $('.jqx-grid-validation').css('display', 'none');
                    //     // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                    // }
        
        
                    // if (this.myGrid.getcelltext(i, 'cost_Center') == '') {
                    //     this.myGrid.showvalidationpopup(i, 'cost_Center', 'Enter valid  Cost Center');
                    // }
                    // else {
        
                    //     // $('.jqx-grid-validation').css('display', 'none');
                    //     // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                    // }
        
                    // if (this.myGrid.getcelltext(i, 'geozone') == '') {
                    //     this.myGrid.showvalidationpopup(i, 'geozone', 'Enter valid  geozone');
                    // }
                    // else {
        
                    //     // $('.jqx-grid-validation').css('display', 'none');
                    //     // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                    // }
        
                    // if (this.myGrid.getcelltext(i, 'facility') == '') {
                    //     this.myGrid.showvalidationpopup(i, 'facility', 'Enter valid  facility');
                    // }
                    // else {
        
                    //     // $('.jqx-grid-validation').css('display', 'none');
                    //     // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                    // }
        
        
                    // if (this.myGrid.getcelltext(i, 'employee_Class') == '') {
                    //     this.myGrid.showvalidationpopup(i, 'employee_Class', 'Enter valid  Employee Class');
                    // }
                    // else {
        
                    //     // $('.jqx-grid-validation').css('display', 'none');
                    //     // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                    // }
        
                    // if (this.myGrid.getcelltext(i, 'employee_Type') == '') {
                    //     this.myGrid.showvalidationpopup(i, 'employee_Type', 'Enter valid  Employee Type');
                    // }
                    // else {
        
                    //     // $('.jqx-grid-validation').css('display', 'none');
                    //     // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                    // }
        
                    // if (this.myGrid.getcelltext(i, 'job_Family') == '') {
                    //     this.myGrid.showvalidationpopup(i, 'job_Family', 'Enter valid  Job Family');
                    // }
                    // else {
        
                    //     // $('.jqx-grid-validation').css('display', 'none');
                    //     // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                    // }
        
                    // if (this.myGrid.getcelltext(i, 'job_Code') == '') {
                    //     this.myGrid.showvalidationpopup(i, 'job_Code', 'Enter valid  Job Code');
                    // }
                    // else {
        
                    //     // $('.jqx-grid-validation').css('display', 'none');
                    //     // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                    // }
        
                    // if (this.myGrid.getcelltext(i, 'variable_Pay_Plan_Type') == '') {
                    //     this.myGrid.showvalidationpopup(i, 'variable_Pay_Plan_Type', 'Enter valid  Variable Pay Plan Type');
                    // }
                    // else {
        
                    //     // $('.jqx-grid-validation').css('display', 'none');
                    //     // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                    // }
        
        
                    // if (this.myGrid.getcelltext(i, 'reporting_Manager') == '') {
                    //     this.myGrid.showvalidationpopup(i, 'reporting_Manager', 'Enter valid  Reporting Manager');
                    // }
                    // else {
        
                    //     // $('.jqx-grid-validation').css('display', 'none');
                    //     // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                    // }
        
        
                    // if (this.myGrid.getcelltext(i, 'bu_Hr_Spoc') == '') {
                    //     this.myGrid.showvalidationpopup(i, 'bu_Hr_Spoc', 'Enter valid  BU HR SPOC');
                    // }
                    // else {
        
                    //     // $('.jqx-grid-validation').css('display', 'none');
                    //     // $('.jqx-grid-validation-arrow-up').css('display', 'none');
                    // }
        
        
                }
        */

        // if(value4=='')
        // {


        // }
        // else {
        //     if (/[^0-9a-bA-B\s]/gi.test(value4)) {

        //         this.httpClient.post('http://13.90.80.185:3337/user/validate_api/', value2)
        //             .map((response: any) => response)
        //             .do(data => this.datajsonsave = data)
        //             .subscribe(() => {  this.final(this.datajsonsave.Output.error) }


        //             )

        //     }

        //     else {
        //         $("#error-btn").click()
        //     }


        // }
    }

    Cellclick(event: any): void {
        if (event.args.columnindex == 18) {
            this.myGrid.addrow(1, {});
        }
        console.log(event.args.columnindex);

    }

    enterclick() {

        this.myGrid.addrow(1, {});

    }

    hoverchange() {
        this.printposition = this.myGrid.getrows();
        console.log(this.printposition);
    }

    selectchange(event: any) {

        console.log(event);
        this.deletedata = (event.target.value);
        this.change = (event.target.options.selectedIndex);
        console.log(this.change);
        //console.log(this.printposition.PositionId)
        // this.myGrid.deleterow(this.change);
    }

    deleterow() {

        //console.log(position.uid)


        let value6 = this.myGrid.getselectedrowindex();
        console.log(value6);

        this.myGrid.deleterow(this.deletedata);


        if (this.change == 1) {
            this.myGrid.addrow(1, {});
        }

        $('#showdiv').css('display', 'none');

        // let value4 = this.myGrid.getrows();


        /* this.httpClient
             .post('http://13.90.80.185:3339/user/xml/',value2)
             .subscribe()

         window.location.reload();*/

    }

    newrow() {

        this.myGrid.addrow(1, { posi: 'P234' });

    }

    check(value) {
        console.log(value);

        if (value == 1) {
            let value = this.myGrid.exportdata('csv', 'grid12345');
        }
        if (value == 2) {
            let value = this.myGrid.exportdata('xls', 'grid12345');
        }
    }


    opendiv() {
        $('#opendiv').css('display', 'block');
    }

}
