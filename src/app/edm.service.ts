import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Configration } from './edm-config' 
import { HttpClient } from '@angular/common/http';

@Injectable({
	providedIn: 'root'
})
export class EdmService { 
  public url : any = Configration.weburl;
  constructor(public httpClient:HttpClient){}
	httpPost(functionUrl,event){
    return new Promise((resolve,reject)=>{
      
		this.httpClient
        .post(this.url + functionUrl, event)
        .map((response: any) => response)
        // .do(data => this.employeecodejsonsave = data)
        .subscribe((s) =>resolve(s))
        
    })
	}

}
