import { Component, OnInit } from '@angular/core';
import { EdmService } from '../edm.service';
declare var $;
declare var bootbox;

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  visibleTab = 'positionform';
  public employeecodejsonsave; 
  
  datashowcol = [
    'Position ID', 'Status', 'Effective Date', 'Legal Entity', 'Bu/SSU', 'Lob Segment', 'Vertical', 'Sub  Vertical', 'Cost center', 'Geozone', 'Facility', 'Employee Class', 'Employee Type', 'Job Family', 'Job Code', 'Internal Specialization', 'Variable Pay Plan Type', 'Reporting Manager', 'Bu Hr SPOC'
  ];
  positioncodejsonsave = {
    'Output': []
  };
  positioncodejson:any;

  statusjson = {
    'Output': [
      'Active', 'Inactive'
    ]
  };

  qwerty = {
    'attributes': ['position_Id'],
    'values': ['values']
  };
  constructor(public edmService: EdmService) {

  }

  switchTo(value) {
    this.visibleTab = value;
  }

  ngOnInit() {
  }

  employeecodesend(event) {

    if ($("#z").val()) {

      console.log(event)
      $("#z").css("borderColor", "lightgray");
      $(".invalid-feedback").css("visibility", "hidden");
      this.edmService.httpPost('user/login/', event)
        .then((s) => {
          this.employeecodejsonsave = s;
          this.printemail()
        })

    }
    else {
      $("#z").css("borderColor", "red");
      $(".invalid-feedback").css("visibility", "visible");
      $("#zz").val("")
    }

  }

  printemail() {
 
    $('#zz').val(this.employeecodejsonsave.Employee_information.Employee_email)

    if ($('#zz').val()) {
      $("#myInput").removeAttr("disabled");
      this.cookie_popup();
    }
    else {
      console.log("Hello")
    }
    this.edmService.httpPost('user/json/', this.qwerty).then((s) => {
      this.positioncodejson = s;
      // this.positioncodejsonsave = s;
    })


  }
  cookie_popup() {
    var popup = document.getElementById("exampleModal");
    // console.log(popup)
    // popup.classList.add("in");
  }


}
