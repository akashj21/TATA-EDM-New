import { Component, OnInit, ViewChild, Input, EventEmitter, OnChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { jqxDateTimeInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdatetimeinput'
import { Angular5Csv } from 'angular5-csv/Angular5-csv';

export { NgbDatepicker } from '@ng-bootstrap/ng-bootstrap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do'
import { EdmService } from '../edm.service';



declare var $;

@Component({
  selector: 'app-position-form',
  templateUrl: './position-form.component.html',
  styleUrls: ['./position-form.component.css']
})
export class PositionFormComponent implements OnInit, OnChanges {

  @ViewChild('dateTimeInput') myDateTimeInput: jqxDateTimeInputComponent;
  @Input() positioncodejson: any = new EventEmitter;
  firstreview: boolean = true;

  cook: any;
  cookieValue = 'UNKNOWN';
  le_search: any;
  deletedata: any;
  editvaluetable: any
  checkbox_legal: boolean = false;
  checkbox_bu: boolean = false;
  maxform: boolean = false;
  valid_legal: any;
  valid_bu: any;
  currentsection: any = 'positionsection'
  datashowcol = [
    'Position ID', 'Status', 'Effective Date', 'Legal Entity', 'Bu/SSU', 'Lob Segment', 'Vertical', 'Sub  Vertical', 'Cost center', 'Geozone', 'Facility', 'Employee Class', 'Employee Type', 'Job Family', 'Job Code', 'Internal Specialization', 'Variable Pay Plan Type', 'Reporting Manager', 'Bu Hr SPOC'
  ]
  positioncodejsonsave = {
    'Output': []
  };


  statusjson = {
    'Output': [
      'Active', 'Inactive'
    ]
  };

  qwerty = {
    'attributes': ['position_Id'],
    'values': ['values']
  };


  dataprint = {


    'position_ID_Section': {
      'PositionID': '',
      'Status': '',
      'effectiveDate': ''
    },
    'five_level_hierarchy': {
      'BU_SSU': '',
      'LegalEntity': '',
      'CostCenter': '',
      'LOB_Segment': '',
      'SubVertical': '',
      'Vertical': ''
    },
    'GeoZone_and_Facility': {
      'Facility': '',
      'GeoZone': ''
    },
    'EmployeeClass_and_Type': {
      'EmployeeClass': '',
      'EmployeeType': ''
    },
    'Global_Work_Levels': {
      'InternalSpecialization': '',
      'JobFamily': '',
      'JobCode': '',
      'VariablePayPlanType': ''
    },
    'Reporting_and_BUHR_SPOC': {
      'BUHRSPOC': '',
      'ReportingManagerPosition': ''
    }

  };


  legaldataprint: any;

  geozone = {
    'Output': []

  };

  costcenter: any;

  bussu: any;
  lobsegment: any;
  vertical = {

    'Output': []

  };
  subvertical = {

    'Output': []

  };
  facility = {

    'Output': []

  };
  employeeclass = {

    'Output': []

  };
  employeetype = {

    'Output': []

  };
  jobfamily = {

    'Output': []

  };

  jobcode = {

    'Output': []

  };

  internalspecilization = {

    'Output': []

  };

  variablepay = {

    'Output': []

  };

  reportingmanager = {

    'Output': []

  };

  buhrspoc = {

    'Output': []

  };




  employeecodejsonsave = {

    'Employee_information': {

      'Employee_code': [],
      'Employee_email': []

    }
  }


  positiondata: any = '';
  effectivedatedata: any = '';
  statusdata: any = '';
  jobcodedata: any = '';
  legaldata: any = '';
  budata: any = '';
  lobdata: any = '';
  verticaldata: any = '';
  subvdata: any = '';
  costcenterdata: any = '';
  geozonedata: any = '';
  facilitydata: any = '';
  emptypedata: any = '';
  empclassdata: any = '';
  jobfamilydata: any = '';
  internaldata: any = '';
  variabledata: any = '';
  buhrdata: any = '';
  reportingdata: any = '';
  EmployeeCode: any;
  value: any;
  datasession =
    {
      'key': [],
      'value': []
    };

  datasessiontable =
    {
      'key': [],
      'value': []
    };


  alldataprint: any;

  func(data) {
    console.log('123');

    this.positioncodejsonsave = data;
  }

  /** AKASH MADE CODE HERE */
  public showPositionData: any = false;
  public showActivePositionData: any = false;
  public step1data: any = [];
  constructor(private httpClient: HttpClient,
    public cookieService: CookieService, public edmService: EdmService) {

  }
  ngOnChanges() {
    console.log(this.positioncodejson)
  }
  ngOnInit() {
    // this.cookieValue = this.cookieService.get('employ_code');
    // this.EmployeeCode = this.cookieValue;
    // console.log(this.cookieValue)
    // var ecode = {

    //   'EmployeeCode': this.cookieValue

    // }
    // setTimeout(() => {
    //   this.employeecodesend(ecode);
    // }, 1000);




    // var element1 = document.getElementById("lev");
    // var element2 = document.getElementById("geofac");
    // var element3 = document.getElementById("emplc");
    // var element4 = document.getElementById("repo");
    // var element5 = document.getElementById("glob");


  }


  showme() {
    document.getElementById('position').style.display = 'block';
    document.getElementById('bulk').style.display = 'none';
    document.getElementById('formdatadiv').style.display = 'none';
    var bulk_css = document.getElementById("a2");
    bulk_css.classList.remove("bulk-css");
    var reclass_css = document.getElementById("a1");
    reclass_css.classList.remove("reclass-css");

  }

  hideme() {
    document.getElementById('position').style.display = 'none';
    document.getElementById('formdatadiv').style.display = 'none';
    document.getElementById('bulk').style.display = 'block';
    var bulk_css = document.getElementById("a2");
    bulk_css.classList.add("bulk-css");
    var reclass_css = document.getElementById("a1");
    reclass_css.classList.add("reclass-css");

  }

  white1() {
    document.getElementById('a1').style.color = 'white';

  }

  white2() {
    document.getElementById('a2').style.color = 'white';

  }

  black1() {
    document.getElementById('a1').style.color = 'black';

  }

  black2() {
    document.getElementById('a2').style.color = 'black';

  }


  // a() {
  //   document.getElementById('a-err').style.visibility = 'hidden';
  //   document.getElementById('myInput').style.borderColor = 'lightgray';
  // }


  // b() {
  //   document.getElementById('b-err').style.visibility = 'hidden';
  //   document.getElementById('myInput-1').style.borderColor = 'lightgray';
  // }


  // c() {
  //   document.getElementById('c-err').style.visibility = 'hidden';
  //   document.getElementById('myInput-2').style.borderColor = 'lightgray';
  // }


  // d() {
  //   document.getElementById('d-err').style.visibility = 'hidden';
  //   document.getElementById('myInput-3').style.borderColor = 'lightgray';
  // }
  // dc() {
  //   document.getElementById('d-err').style.visibility = 'hidden';
  //   document.getElementById('myInput-3').style.borderColor = 'lightgray';
  // }

  // e() {
  //   document.getElementById('e-err').style.visibility = 'hidden';
  //   document.getElementById('myInput-4').style.borderColor = 'lightgray';
  // }


  // f() {
  //   document.getElementById('f-err').style.visibility = 'hidden';
  //   document.getElementById('myInput-5').style.borderColor = 'lightgray';
  // }


  // g() {
  //   document.getElementById('g-err').style.visibility = 'hidden';
  //   document.getElementById('myInput-6').style.borderColor = 'lightgray';
  // }


  // h() {
  //   document.getElementById('h-err').style.visibility = 'hidden';
  //   document.getElementById('myInput-8').style.borderColor = 'lightgray';
  // }


  // i() {
  //   document.getElementById('i-err').style.visibility = 'hidden';
  //   document.getElementById('myInput-9').style.borderColor = 'lightgray';
  // }
  // i1() {
  //   document.getElementById('i1-err').style.visibility = 'hidden';
  //   document.getElementById('myInput-10').style.borderColor = 'lightgray';
  // }


  // j() {
  //   document.getElementById('j-err').style.visibility = 'hidden';
  //   document.getElementById('myInput-11').style.borderColor = 'lightgray';
  // }


  // k() {
  //   document.getElementById('k-err').style.visibility = 'hidden';
  //   document.getElementById('myInput-12').style.borderColor = 'lightgray';
  // }


  // l() {
  //   document.getElementById('l-err').style.visibility = 'hidden';
  //   document.getElementById('myInput-13').style.borderColor = 'lightgray';
  // }


  // m() {
  //   document.getElementById('m-err').style.visibility = 'hidden';
  //   document.getElementById('myInput-14').style.borderColor = 'lightgray';
  // }


  // n() {
  //   document.getElementById('n-err').style.visibility = 'hidden';
  //   document.getElementById('myInput-16').style.borderColor = 'lightgray';
  // }


  // o() {
  //   document.getElementById('o-err').style.visibility = 'hidden';
  //   document.getElementById('myInput-17').style.borderColor = 'lightgray';
  // }


  // p() {
  //   document.getElementById('p-err').style.visibility = 'hidden';
  //   document.getElementById('myInput-18').style.borderColor = 'lightgray';
  // }

  // z() {
  //   document.getElementById('z-err').style.visibility = 'hidden';
  //   document.getElementById('z').style.borderColor = 'lightgray';
  // }


  // zz() {
  //   document.getElementById('zz-err').style.visibility = 'hidden';
  //   document.getElementById('zz').style.borderColor = 'lightgray';
  // }

  position() {


    document.getElementById('formdatadiv').style.display = 'none';
    document.getElementById('position').style.display = 'block';


    $('.step-1').css('display', 'block');
    $('.step-2').css('display', 'none');
    $('.step-3').css('display', 'none');
    $('.step-4').css('display', 'none');
    $('.step-5').css('display', 'none');
    $('.step-6').css('display', 'none');


    var element = document.getElementById("pos");
    var pos_li = document.getElementById("pos_li");
    var lev_li = document.getElementById("lev_li");
    var geofac_li = document.getElementById("geofac_li");
    var emplc_li = document.getElementById("emplc_li");
    var repo_li = document.getElementById("repo_li");
    var glob_li = document.getElementById("glob_li");
    var element1 = document.getElementById("lev");
    var element2 = document.getElementById("geofac");
    var element3 = document.getElementById("emplc");
    var element4 = document.getElementById("repo");
    var element5 = document.getElementById("glob");

    element.classList.remove("inactive1");
    element.classList.add("active1");
    lev_li.classList.remove("current22");
    geofac_li.classList.remove("current22");
    repo_li.classList.remove("current22");
    emplc_li.classList.remove("current22");
    glob_li.classList.remove("current22");
    pos_li.classList.add("current22");
    element1.classList.add("inactive1");
    element2.classList.add("inactive1");
    element3.classList.add("inactive1");
    element5.classList.add("inactive1");
    element4.classList.add("inactive1");
  }



  pop() {
    var flag: boolean = false;
    var a = $('#myInput').val();
    var b = $('#myInput-1').val();
    var c = $('#myInput-2').val();
    var z = $('#z').val();


    this.step1data.push({
      a: a, b: b, c: c, z: z
    })

    if (a && b && c && z) {
      $('.invalid-feedback').parent().find('input').each(function (e, v) {
        if ($(this).val() == '') {
          $(this).next('.invalid-feedback').css({ 'display': 'none', 'visibility': 'hidden' })
        }
      })
      this.currentsection = "levelhierarchy";

      setTimeout(() => {
        $(".form-control").removeAttr("disabled");
        this.le_post();
      }, 100);



      // $('.step-1').css('display', 'none');
      // $('.step-2').css('display', 'block');
      // $('.step-3').css('display', 'none');
      // $('.step-4').css('display', 'none');
      // $('.step-5').css('display', 'none');
      // $('.step-6').css('display', 'none');


      // var element = document.getElementById("pos");
      // var pos_li = document.getElementById("pos_li");
      // var lev_li = document.getElementById("lev_li");
      // var geofac_li = document.getElementById("geofac_li");
      // var emplc_li = document.getElementById("emplc_li");
      // var repo_li = document.getElementById("repo_li");
      // var glob_li = document.getElementById("glob_li");

      // var element1 = document.getElementById("lev");
      // var element2 = document.getElementById("geofac");
      // var element3 = document.getElementById("emplc");
      // var element4 = document.getElementById("repo");
      // var element5 = document.getElementById("glob");

      // element1.classList.remove("inactive1");
      // element1.classList.add("active1");
      // lev_li.classList.add("current22");
      // geofac_li.classList.remove("current22");
      // repo_li.classList.remove("current22");
      // emplc_li.classList.remove("current22");
      // glob_li.classList.remove("current22");
      // pos_li.classList.remove("current22");
      // element.classList.add("inactive1");
      // element2.classList.add("inactive1");
      // element3.classList.add("inactive1");
      // element5.classList.add("inactive1");
      // element4.classList.add("inactive1");

    }
    else {

      $('.invalid-feedback').parent().find('input').each(function (e, v) {
        if ($(this).val() == '') {
          $(this).next('.invalid-feedback').css({ 'display': 'block', 'visibility': 'visible' })
        }
      })
      // if($('.invalid-feedback').parent().find('input').val()=='') {

      // }

      // document.getElementById('a-err').style.visibility = 'visible';
      // document.getElementById('b-err').style.visibility = 'visible';
      // document.getElementById('c-err').style.visibility = 'visible';
      // document.getElementById('z-err').style.visibility = 'visible';

      // document.getElementById('myInput').style.borderColor = 'red';
      // document.getElementById('myInput-1').style.borderColor = 'red';
      // document.getElementById('myInput-2').style.borderColor = 'red';
      // document.getElementById('z').style.borderColor = 'red';

      // var lev_li = document.getElementById("lev_li");
      // lev_li.classList.remove("current");
    }
    // if (a) {
    //   document.getElementById('a-err').style.visibility = 'hidden';
    //   document.getElementById('myInput').style.borderColor = 'lightgray';
    // }

    // if (b) {
    //   document.getElementById('b-err').style.visibility = 'hidden';
    //   document.getElementById('myInput-1').style.borderColor = 'lightgray';
    // }

    // if (c) {
    //   document.getElementById('c-err').style.visibility = 'hidden';
    //   document.getElementById('myInput-2').style.borderColor = 'lightgray';
    // }
    // if (z) {
    //   document.getElementById('z-err').style.visibility = 'hidden';
    //   document.getElementById('z').style.borderColor = 'lightgray';
    // }

    return flag;
  }

  pop1() {

    var pop1flag: boolean = false

    if (this.step1data[0].a && this.step1data[0].b && this.step1data[0].c && this.step1data[0].z) {

      var d = $('#myInput-3').val();
      var e = $('#myInput-4').val();
      var f = $('#myInput-5').val();
      var g = $('#myInput-6').val();
      var h = $('#myInput-8').val(); 
      var file = $(".k-file").val()

      if (file == 0) {
        console.log("file");
      }
      else {
        console.log("Not File");
      }

      if (d && e && f && g && h) {
        this.step1data.push({
          d: d, e: e, f: f, g: g, h: h
        })
        console.log(this.step1data)
        if (this.checkbox_legal == false && this.checkbox_bu == false) {

          // document.getElementById('paa-err').style.visibility = 'hidden';
          // document.getElementById('drg-err').style.visibility = 'hidden';
          // document.getElementById('d-err').style.visibility = 'hidden';
          // document.getElementById('myInput-3').style.borderColor = 'lightgray';

          // document.getElementById('e-err').style.visibility = 'hidden';
          // document.getElementById('myInput-4').style.borderColor = 'lightgray';

          // document.getElementById('f-err').style.visibility = 'hidden';
          // document.getElementById('myInput-5').style.borderColor = 'lightgray';

          // document.getElementById('g-err').style.visibility = 'hidden';
          // document.getElementById('myInput-6').style.borderColor = 'lightgray';

          // document.getElementById('h-err').style.visibility = 'hidden';
          // document.getElementById('myInput-8').style.borderColor = 'lightgray';

          // $('.step-1').css('display', 'none');
          // $('.step-2').css('display', 'none');
          // $('.step-3').css('display', 'block');
          // $('.step-4').css('display', 'none');
          // $('.step-5').css('display', 'none');
          // $('.step-6').css('display', 'none');


          // var pos_li = document.getElementById("pos_li");
          // var lev_li = document.getElementById("lev_li");
          // var geofac_li = document.getElementById("geofac_li");
          // var emplc_li = document.getElementById("emplc_li");
          // var repo_li = document.getElementById("repo_li");
          // var glob_li = document.getElementById("glob_li");

          // var element = document.getElementById("pos");

          // var element1 = document.getElementById("lev");
          // var element2 = document.getElementById("geofac");
          // var element3 = document.getElementById("emplc");
          // var element4 = document.getElementById("repo");
          // var element5 = document.getElementById("glob");


          // lev_li.classList.remove("current22");
          // geofac_li.classList.add("current22");
          // repo_li.classList.remove("current22");
          // emplc_li.classList.remove("current22");
          // glob_li.classList.remove("current22");
          // pos_li.classList.remove("current22");

          // element2.classList.remove("inactive1");
          // element2.classList.add("active1");

          // element1.classList.add("inactive1");
          // element.classList.add("inactive1");
          // element3.classList.add("inactive1");
          // element5.classList.add("inactive1");
          // element4.classList.add("inactive1");
          $('.invalid-feedback').parent().find('input').each(function (e, v) {
            if ($(this).val() == '') {
              $(this).next('.invalid-feedback').css({ 'display': 'none', 'visibility': 'hidden' })
            }
          })
          this.currentsection = "geozonefacility";

        }


        else {
          var paa =
            document.getElementById('paa') as HTMLInputElement;
          if (paa.checked) {
            document.getElementById('paa-err').style.visibility = 'hidden';
            if (file == 0 && $('.k-file-validation-message').val() != "") {
              $('.invalid-feedback').parent().find('input').each(function (e, v) {
                if ($(this).val() == '') {
                  $(this).next('.invalid-feedback').css({ 'display': 'none', 'visibility': 'hidden' })
                }
              })
              this.currentsection = "geozonefacility";

              // document.getElementById('drg-err').style.visibility = 'hidden';
              // document.getElementById('d-err').style.visibility = 'hidden';
              // document.getElementById('myInput-3').style.borderColor = 'lightgray';
              // document.getElementById('paa-err').style.visibility = 'hidden';
              // document.getElementById('drg-err').style.visibility = 'hidden';
              // document.getElementById('e-err').style.visibility = 'hidden';
              // document.getElementById('myInput-4').style.borderColor = 'lightgray';

              // document.getElementById('f-err').style.visibility = 'hidden';
              // document.getElementById('myInput-5').style.borderColor = 'lightgray';

              // document.getElementById('g-err').style.visibility = 'hidden';
              // document.getElementById('myInput-6').style.borderColor = 'lightgray';

              // document.getElementById('h-err').style.visibility = 'hidden';
              // document.getElementById('myInput-8').style.borderColor = 'lightgray';

              // $('.step-1').css('display', 'none');
              // $('.step-2').css('display', 'none');
              // $('.step-3').css('display', 'block');
              // $('.step-4').css('display', 'none');
              // $('.step-5').css('display', 'none');
              // $('.step-6').css('display', 'none');


              // var pos_li = document.getElementById("pos_li");
              // var lev_li = document.getElementById("lev_li");
              // var geofac_li = document.getElementById("geofac_li");
              // var emplc_li = document.getElementById("emplc_li");
              // var repo_li = document.getElementById("repo_li");
              // var glob_li = document.getElementById("glob_li");

              // var element = document.getElementById("pos");

              // var element1 = document.getElementById("lev");
              // var element2 = document.getElementById("geofac");
              // var element3 = document.getElementById("emplc");
              // var element4 = document.getElementById("repo");
              // var element5 = document.getElementById("glob");


              // lev_li.classList.remove("current22");
              // geofac_li.classList.add("current22");
              // repo_li.classList.remove("current22");
              // emplc_li.classList.remove("current22");
              // glob_li.classList.remove("current22");
              // pos_li.classList.remove("current22");

              // element2.classList.remove("inactive1");
              // element2.classList.add("active1");

              // element1.classList.add("inactive1");
              // element.classList.add("inactive1");
              // element3.classList.add("inactive1");
              // element5.classList.add("inactive1");
              // element4.classList.add("inactive1");
            }

            else {
              document.getElementById('drg-err').style.visibility = 'visible';

            }
          }
          else {
            document.getElementById('paa-err').style.visibility = 'visible';

          }

        }


        pop1flag = true
      }
      else {
        $('.invalid-feedback').parent().find('input').each(function (e, v) {
          if ($(this).val() == '') {
            $(this).next('.invalid-feedback').css({ 'display': 'block', 'visibility': 'visible' })
          }
        })

        // document.getElementById('d-err').style.visibility = 'visible';
        // document.getElementById('myInput-3').style.borderColor = 'red';
        // document.getElementById('e-err').style.visibility = 'visible';
        // document.getElementById('myInput-4').style.borderColor = 'red';
        // document.getElementById('f-err').style.visibility = 'visible';
        // document.getElementById('myInput-5').style.borderColor = 'red';
        // document.getElementById('g-err').style.visibility = 'visible';
        // document.getElementById('myInput-6').style.borderColor = 'red';
        // document.getElementById('h-err').style.visibility = 'visible';
        // document.getElementById('myInput-8').style.borderColor = 'red';

      }

      // if (d) {
      //   document.getElementById('d-err').style.visibility = 'hidden';
      //   document.getElementById('myInput-3').style.borderColor = 'lightgray';
      // }
      // if (e) {
      //   document.getElementById('e-err').style.visibility = 'hidden';
      //   document.getElementById('myInput-4').style.borderColor = 'lightgray';
      // }
      // if (f) {
      //   document.getElementById('f-err').style.visibility = 'hidden';
      //   document.getElementById('myInput-5').style.borderColor = 'lightgray';
      // }
      // if (g) {
      //   document.getElementById('g-err').style.visibility = 'hidden';
      //   document.getElementById('myInput-6').style.borderColor = 'lightgray';
      // }
      // if (h) {
      //   document.getElementById('h-err').style.visibility = 'hidden';
      //   document.getElementById('myInput-8').style.borderColor = 'lightgray';
      // }

    }


    return pop1flag
  }

  pop2() {
    var pop2flag: boolean = false
    if (this.pop1()) {



      console.log("pop2");
      var i = $('#myInput-9').val();
      var i1 = $('#myInput-10').val();


      if (i && i1) {

        document.getElementById('i-err').style.visibility = 'hidden';
        document.getElementById('myInput-9').style.borderColor = 'lightgray';
        document.getElementById('q-err').style.visibility = 'hidden';
        document.getElementById('myInput-10').style.borderColor = 'lightgray';

        $('.step-1').css('display', 'none');
        $('.step-2').css('display', 'none');
        $('.step-3').css('display', 'none');
        $('.step-4').css('display', 'block');
        $('.step-5').css('display', 'none');
        $('.step-6').css('display', 'none');


        var pos_li = document.getElementById("pos_li");
        var lev_li = document.getElementById("lev_li");
        var geofac_li = document.getElementById("geofac_li");
        var emplc_li = document.getElementById("emplc_li");
        var repo_li = document.getElementById("repo_li");
        var glob_li = document.getElementById("glob_li");

        var element = document.getElementById("pos");

        var element1 = document.getElementById("lev");
        var element2 = document.getElementById("geofac");
        var element3 = document.getElementById("emplc");
        var element4 = document.getElementById("repo");
        var element5 = document.getElementById("glob");

        lev_li.classList.remove("current22");
        geofac_li.classList.remove("current22");
        repo_li.classList.remove("current22");
        emplc_li.classList.add("current22");
        glob_li.classList.remove("current22");
        pos_li.classList.remove("current22");

        element3.classList.remove("inactive1");
        element3.classList.add("active1");

        element1.classList.add("inactive1");
        element2.classList.add("inactive1");
        element.classList.add("inactive1");
        element5.classList.add("inactive1");
        element4.classList.add("inactive1");

        pop2flag = true

      }
      else {

        document.getElementById('i-err').style.visibility = 'visible';
        document.getElementById('myInput-9').style.borderColor = 'red';

        document.getElementById('q-err').style.visibility = 'visible';
        document.getElementById('myInput-10').style.borderColor = 'red';

      }

      if (i) {
        document.getElementById('i-err').style.visibility = 'hidden';
        document.getElementById('myInput-9').style.borderColor = 'lightgray';
      }
      if (i1) {
        document.getElementById('q-err').style.visibility = 'hidden';
        document.getElementById('myInput-10').style.borderColor = 'lightgray';
      }
    }

    return pop2flag
  }


  pop3() {

    var pop3flag: boolean = false

    if (this.pop2()) {


      console.log("pop3");
      var j = $('#myInput-11').val();
      var k = $('#myInput-12').val();


      if (j && k) {
        document.getElementById('j-err').style.visibility = 'hidden';
        document.getElementById('myInput-11').style.borderColor = 'lightgray';
        document.getElementById('k-err').style.visibility = 'hidden';
        document.getElementById('myInput-12').style.borderColor = 'lightgray';


        $('.step-1').css('display', 'none');
        $('.step-2').css('display', 'none');
        $('.step-3').css('display', 'none');
        $('.step-4').css('display', 'none');
        $('.step-5').css('display', 'block');
        $('.step-6').css('display', 'none');

        var pos_li = document.getElementById("pos_li");
        var lev_li = document.getElementById("lev_li");
        var geofac_li = document.getElementById("geofac_li");
        var emplc_li = document.getElementById("emplc_li");
        var repo_li = document.getElementById("repo_li");
        var glob_li = document.getElementById("glob_li");


        lev_li.classList.remove("current22");
        geofac_li.classList.remove("current22");
        repo_li.classList.remove("current22");
        emplc_li.classList.remove("current22");
        glob_li.classList.add("current22");
        pos_li.classList.remove("current22");


        var element = document.getElementById("pos");

        var element1 = document.getElementById("lev");
        var element2 = document.getElementById("geofac");
        var element3 = document.getElementById("emplc");
        var element4 = document.getElementById("repo");
        var element5 = document.getElementById("glob");

        element5.classList.remove("inactive1");
        element5.classList.add("active1");

        element1.classList.add("inactive1");
        element2.classList.add("inactive1");
        element3.classList.add("inactive1");
        element4.classList.add("inactive1");
        element.classList.add("inactive1");
        pop3flag = true;

      }
      else {


        document.getElementById('j-err').style.visibility = 'visible';
        document.getElementById('myInput-11').style.borderColor = 'red';
        document.getElementById('k-err').style.visibility = 'visible';
        document.getElementById('myInput-12').style.borderColor = 'red';
      }
      if (j) {
        document.getElementById('j-err').style.visibility = 'hidden';
        document.getElementById('myInput-11').style.borderColor = 'lightgray';
      }
      if (k) {
        document.getElementById('k-err').style.visibility = 'hidden';
        document.getElementById('myInput-12').style.borderColor = 'lightgray';
      }
    }

    return pop3flag

  }


  pop4() {
    var pop4flag: boolean = false;
    if (this.pop3()) {


      console.log("pop4");
      var l = $('#myInput-13').val();
      var m = $('#myInput-14').val();
      var n = $('#myInput-16').val();


      if (l && m && n) {

        document.getElementById('l-err').style.visibility = 'hidden';
        document.getElementById('myInput-13').style.borderColor = 'lightgray';

        document.getElementById('m-err').style.visibility = 'hidden';
        document.getElementById('myInput-14').style.borderColor = 'lightgray';
        document.getElementById('n-err').style.visibility = 'hidden';
        document.getElementById('myInput-16').style.borderColor = 'lightgray';

        $('.step-1').css('display', 'none');
        $('.step-2').css('display', 'none');
        $('.step-3').css('display', 'none');
        $('.step-4').css('display', 'none');
        $('.step-5').css('display', 'none');
        $('.step-6').css('display', 'block');

        var pos_li = document.getElementById("pos_li");
        var lev_li = document.getElementById("lev_li");
        var geofac_li = document.getElementById("geofac_li");
        var emplc_li = document.getElementById("emplc_li");
        var repo_li = document.getElementById("repo_li");
        var glob_li = document.getElementById("glob_li");


        lev_li.classList.remove("current22");
        geofac_li.classList.remove("current22");
        repo_li.classList.add("current22");
        emplc_li.classList.remove("current22");
        glob_li.classList.remove("current22");
        pos_li.classList.remove("current22");


        var element = document.getElementById("pos");

        var element1 = document.getElementById("lev");
        var element2 = document.getElementById("geofac");
        var element3 = document.getElementById("emplc");
        var element4 = document.getElementById("repo");
        var element5 = document.getElementById("glob");

        element4.classList.remove("inactive1");
        element4.classList.add("active1");
        element4.classList.add("current");

        element1.classList.add("inactive1");
        element2.classList.add("inactive1");
        element3.classList.add("inactive1");
        element5.classList.add("inactive1");
        element.classList.add("inactive1");

        pop4flag = true

      }
      else {

        document.getElementById('l-err').style.visibility = 'visible';
        document.getElementById('myInput-13').style.borderColor = 'red';
        document.getElementById('m-err').style.visibility = 'visible';
        document.getElementById('myInput-14').style.borderColor = 'red';
        document.getElementById('n-err').style.visibility = 'visible';
        document.getElementById('myInput-16').style.borderColor = 'red';

      }
      if (l) {
        document.getElementById('l-err').style.visibility = 'hidden';
        document.getElementById('myInput-13').style.borderColor = 'lightgray';
      }
      if (m) {
        document.getElementById('m-err').style.visibility = 'hidden';
        document.getElementById('myInput-14').style.borderColor = 'lightgray';
      }
      if (n) {
        document.getElementById('n-err').style.visibility = 'hidden';
        document.getElementById('myInput-16').style.borderColor = 'lightgray';
      }
    }

  }


  search_v0(id) {
    var t1 = $('#' + 'pi' + id).text();
    $('#myInput').val(t1);
    this.positiondata = t1;
    var qwe = {
      'position_Id': t1
    };
    this.edmService.httpPost('user/positionId_api/', qwe).then((s: any) => {
      this.effectivedatedata = s.position_ID_Section.effectiveDate.substr(0, 10)
      this.positiondata = s.position_ID_Section.PositionID;
      this.statusdata = s.position_ID_Section.Status;
      $('#myInput-1').val(s.position_ID_Section.effectiveDate.substr(0, 10));
      this.costcenterdata = s.five_level_hierarchy.CostCenter;
      this.jobcodedata = s.Global_Work_Levels.JobCode;
      this.legaldata = s.five_level_hierarchy.LegalEntity;
      this.valid_legal = s.five_level_hierarchy.LegalEntity;
      this.budata = s.five_level_hierarchy.BU_SSU;
      this.valid_bu = s.five_level_hierarchy.BU_SSU;
      this.lobdata = s.five_level_hierarchy.LOB_Segment;
      this.verticaldata = s.five_level_hierarchy.Vertical;
      this.subvdata = s.five_level_hierarchy.SubVertical;
      this.geozonedata = s.GeoZone_and_Facility.GeoZone;
      this.facilitydata = s.GeoZone_and_Facility.Facility;
      this.emptypedata = s.EmployeeClass_and_Type.EmployeeType;
      this.empclassdata = s.EmployeeClass_and_Type.EmployeeClass;
      this.jobfamilydata = s.Global_Work_Levels.JobFamily;
      this.internaldata = s.Global_Work_Levels.InternalSpecialization;
      this.variabledata = s.Global_Work_Levels.VariablePayPlanType;
      this.buhrdata = s.Reporting_and_BUHR_SPOC.BUHRSPOC;
      this.reportingdata = s.Reporting_and_BUHR_SPOC.ReportingManagerPosition;
      $(".form-control").removeAttr("disabled");
      /* $("#myInput-2").removeAttr("disabled");
       $("#myInput-3").removeAttr("disabled");
       $("#myInput-4").removeAttr("disabled");
       $("#myInput-5").removeAttr("disabled");
       $("#myInput-6").removeAttr("disabled");
       $("#myInput-7").removeAttr("disabled");
       $("#myInput-8").removeAttr("disabled");
       $("#myInput-9").removeAttr("disabled");
       $("#myInput-10").removeAttr("disabled");
       $("#myInput-11").removeAttr("disabled");
       $("#myInput-12").removeAttr("disabled");
       $("#myInput-13").removeAttr("disabled");
       $("#myInput-14").removeAttr("disabled");
       $("#myInput-15").removeAttr("disabled");
       $("#myInput-16").removeAttr("disabled");
       $("#myInput-17").removeAttr("disabled");
       $("#myInput-18").removeAttr("disabled");*/
    })




  }

  search_v1(id1) {

    this.effectivedatedata = this.dataprint.position_ID_Section.effectiveDate.substr(0, 10)

    console.log(id1);
    var t2 = $('#' + id1).text();
    this.effectivedatedata = t2;


    var q = $('#d').value
    var qw = JSON.stringify(q)
    this.effectivedatedata = qw


    $('#myInput-1').val(t2);


  }

  search_v2(id2) {
    var t3 = $('#' + id2).text();
    $('#myInput-2').val(t3);
    this.statusdata = t3;
    this.showActivePositionData = false;
  }

  search_v3(id3) {

    console.log(id3);

    var t4 = $('#' + 'le' + id3).text();
    if (this.valid_legal != t4) {
      this.checkbox_legal = true;
      console.log(this.checkbox_legal, this.checkbox_bu);
    }
    else {
      this.checkbox_legal = false;
      console.log(this.checkbox_legal, this.checkbox_bu);
    }
    $('#myInput-3').val(t4);
    this.legaldata = t4;


    $('#myInput-8').val('');
    $('#myInput-9').val('');
    $('#myInput-10').val('');




  }

  search_v4(id4) {
    console.log(id4);
    var t5 = $('#' + 'bu' + id4).text();
    if (this.valid_bu != t5) {
      this.checkbox_bu = true;
      console.log(this.checkbox_legal, this.checkbox_bu);
    }
    else {
      this.checkbox_bu = false;
      console.log(this.checkbox_legal, this.checkbox_bu);
    }
    $('#myInput-4').val(t5);
    console.log(t5);
    this.budata = t5;



    $('#myInput-5').val('');
    $('#myInput-6').val('');
    $('#myInput-7').val('');
    this.subvdata = ''

    $('#myInput-6').attr('disabled', 'true');
    $('#myInput-7').attr('disabled', 'true');


  }

  search_v5(id5) {
    console.log(id5);
    $('#myInput-6').removeAttr('disabled');
    var t6 = $('#' + 'lob' + id5).text();
    $('#myInput-5').val(t6);
    this.lobdata = t6;

    $('#myInput-6').val('');
    $('#myInput-7').val('');
    $('#myInput-7').attr('disabled', 'true');
  }

  search_v6(id6) {
    console.log(id6);
    $('#myInput-7').removeAttr('disabled');
    var t7 = $('#' + 'ver' + id6).text();
    $('#myInput-6').val(t7);
    this.verticaldata = t7;
    $('#myInput-7').val('');
  }

  search_v7(id7) {
    console.log(id7);
    var t8 = $('#' + 'sub' + id7).text();
    $('#myInput-7').val(t8);
    this.subvdata = t8;
  }

  search_v8(id8) {
    console.log(id8);
    var t9 = $('#' + 'cos' + id8).text();
    $('#myInput-8').val(t9);
    this.costcenterdata = t9;

  }

  search_v9(id9) {
    console.log(id9);
    var t10 = $('#' + 'ge' + id9).text();
    console.log(t10);
    $('#myInput-9').val(t10);

    this.geozonedata = t10;

  }

  search_v10(id10) {
    console.log(id10);
    var t11 = $('#' + 'fa' + id10).text();
    $('#myInput-10').val(t11);
    this.facilitydata = t11;
  }

  search_v11(id11) {
    console.log(id11);
    var t12 = $('#' + 'cl' + id11).text();
    $('#myInput-11').val(t12);
    this.empclassdata = t12;
    $('#myInput-12').val('');
  }

  search_v12(id12) {
    console.log(id12);
    var t13 = $('#' + 'ty' + id12).text();
    $('#myInput-12').val(t13);
    this.emptypedata = t13;
  }

  search_v13(id) {
    console.log(id);
    var t14 = $('#' + 'jf' + id).text();
    $('#myInput-13').val(t14);
    this.jobfamilydata = t14;

    $('#myInput-15').val('');
    $('#myInput-14').val('');
  }

  search_v14(id) {
    console.log(id);
    var t15 = $('#' + 'jc' + id).text();
    $('#myInput-14').val(t15);
    this.jobcodedata = t15;
  }

  search_v15(id) {
    console.log(id);
    var t16 = $('#' + 'is' + id).text();
    $('#myInput-15').val(t16);
    this.internaldata = t16;
  }

  search_v16(id) {
    console.log(id);
    var t17 = $('#' + 'vpp' + id).text();
    $('#myInput-16').val(t17);
    this.variabledata = t17;
  }

  search_v17(id) {
    console.log(id);
    var t18 = $('#' + 'rm' + id).text();
    $('#myInput-17').val(t18);
    this.reportingdata = t18;
  }

  search_v18(id) {
    console.log(id);
    var t19 = $('#' + 'bhs' + id).text();
    $('#myInput-18').val(t19);
    this.buhrdata = t19;
  }

  le_post() {
    var qwe = {
      'attributes': ['legal_Entity'],
      'values': ['values']
    };
    this.edmService.httpPost('user/json/', qwe)
      .then((s: any) => {
        this.legaldataprint = s
      })
      .then(() => this.bu_post())
      .then(() => this.lob_post()) 
      .then(() => this.vertical_post())
      .then(() => this.subv_post())
      .then(() => this.cost_post())

  }


  bu_post() {
    var qwe = {
      'attributes': ['bu_Ssu'],
      'values': ['values']
    };
    this.edmService.httpPost('user/json/', qwe).then((s: any) => {
      this.bussu = s;
    })
  }

  lob_post() {
    var qwe = {
      'attributes': ['bu_Ssu', 'lob_Segment'],
      'values': [this.budata, 'values']
    };
    this.edmService.httpPost('user/json/', qwe).then((s: any) => {
      console.log(s)
      this.lobsegment = s;
    })


  }

  vertical_post() {
    var qwe = {
      'attributes': ['bu_Ssu', 'lob_Segment', 'vertical'],
      'values': [this.budata, this.lobdata, 'values']
    };
    this.edmService.httpPost('user/json/', qwe)
      .then((s: any) => {
        this.vertical = s
      })
  }


  subv_post() {
    var qwe = {
      'attributes': ['bu_Ssu', 'lob_Segment', 'vertical', 'sub_Vertical'],
      'values': [this.budata, this.lobdata, this.verticaldata, 'values']
    };
    this.edmService.httpPost('user/json/', qwe)
      .then((s: any) => {
        this.subvertical = s
      })

  }


  cost_post() {
    $("#loader7").css("visibility", "visible");
    var qwe = {
      'attributes': ['legal_Entity', 'cost_Center'],
      'values': [this.legaldata, 'values']
    };
    this.edmService.httpPost('user/json/', qwe)
      .then((s: any) => {
        this.costcenter = s
        console.log(s);
      })

  }



  geo_post() {
    $("#loader8").css("visibility", "visible");

    var qwe = {
      'attributes': ['legal_Entity', 'geozone'],
      'values': [this.legaldata, 'values']
    };
    this.httpClient
      .post('http://13.90.80.185:3337/user/json/', qwe)
      .map((response: any) => response)
      .do(data => this.geozone = data)
      .subscribe(() => {
        $("#loader8").css("visibility", "hidden");
        console.log(this.geozone);
        // this.i();

      });

  }





  facility_post() {
    $("#loader9").css("visibility", "visible");
    var qwe = {
      'attributes': ['facility'],
      'values': ['values']
    };
    this.httpClient
      .post('http://13.90.80.185:3337/user/json/', qwe)
      .map((response: any) => response)
      .do(data => this.facility = data)
      .subscribe(() => {
        $("#loader9").css("visibility", "hidden");
        console.log(this.facility);

      });
  }

  empclass_post() {
    $("#loader10").css("visibility", "visible");
    var qwe = {
      'attributes': ['employee_Class'],
      'values': ['values']
    };
    this.httpClient
      .post('http://13.90.80.185:3337/user/json/', qwe)
      .map((response: any) => response)
      .do(data => this.employeeclass = data)
      .subscribe(() => {
        $("#loader10").css("visibility", "hidden");
        console.log(this.employeeclass);
        // this.j();

      });
  }

  emptype_post() {
    $("#loader11").css("visibility", "visible");
    var qwe = {
      'attributes': ['employee_Class', 'employee_Type'],
      'values': [this.empclassdata, 'values']
    };
    this.httpClient
      .post('http://13.90.80.185:3337/user/json/', qwe)
      .map((response: any) => response)
      .do(data => this.employeetype = data)
      .subscribe(() => {
        $("#loader11").css("visibility", "hidden");
        console.log(this.employeetype);
        // this.k();

      });
  }

  jobfamily_post() {
    $("#loader12").css("visibility", "visible");
    var qwe = {
      'attributes': ['job_Family'],
      'values': ['values']
    };
    this.httpClient
      .post('http://13.90.80.185:3337/user/json/', qwe)
      .map((response: any) => response)
      .do(data => this.jobfamily = data)
      .subscribe(() => {
        $("#loader12").css("visibility", "hidden");
        console.log(this.jobfamily);
        // this.l();

      });
  }

  jobcode_post() {
    $("#loader13").css("visibility", "visible");
    var qwe = {
      'attributes': ['job_Family', 'job_Code'],
      'values': [this.jobfamilydata, 'values']
    };
    this.httpClient
      .post('http://13.90.80.185:3337/user/json/', qwe)
      .map((response: any) => response)
      .do(data => this.jobcode = data)
      .subscribe(() => {
        $("#loader13").css("visibility", "hidden");
        console.log(this.jobcode);
        // this.m();

      });
  }

  internal_post() {
    $("#loader14").css("visibility", "visible");
    var qwe = {
      'attributes': ['job_Family', 'internal_Specialization'],
      'values': [this.jobfamilydata, 'values']
    };
    this.httpClient
      .post('http://13.90.80.185:3337/user/json/', qwe)
      .map((response: any) => response)
      .do(data => this.internalspecilization = data)
      .subscribe(() => {
        $("#loader14").css("visibility", "hidden");
        console.log(this.internalspecilization);

      });
  }

  buhr_post() {
    $("#loader17").css("visibility", "visible");
    var qwe = {
      'attributes': ['bu_Hr_Spoc'],
      'values': ['values']
    };
    this.httpClient
      .post('http://13.90.80.185:3337/user/json/', qwe)
      .map((response: any) => response)
      .do(data => this.buhrspoc = data)
      .subscribe(() => {
        $("#loader17").css("visibility", "hidden");
        console.log(this.buhrspoc);

      });
  }

  reporting_post() {
    $("#loader16").css("visibility", "visible");
    var qwe = {
      'attributes': ['reporting_Manager'],
      'values': ['values']
    };
    this.httpClient
      .post('http://13.90.80.185:3337/user/json/', qwe)
      .map((response: any) => response)
      .do(data => this.reportingmanager = data)
      .subscribe(() => {
        $("#loader16").css("visibility", "hidden");
        console.log(this.reportingmanager);

      });
  }

  variable_post() {
    $("#loader15").css("visibility", "visible");
    var qwe = {
      'attributes': ['variable_Pay_Plan_Type'],
      'values': ['values']
    };
    this.httpClient
      .post('http://13.90.80.185:3337/user/json/', qwe)
      .map((response: any) => response)
      .do(data => this.variablepay = data)
      .subscribe(() => {
        $("#loader15").css("visibility", "hidden");
        console.log(this.variablepay);

      });
  }


  tick() {

    var paa =
      document.getElementById('paa') as HTMLInputElement;

    if (paa.checked) {

      $('#drg').css('display', 'block');

    }

    else {

      $('#drg').css('display', 'none');

    }


  }


  submitform(event) {


    var o = $('#myInput-17').val();
    var p = $('#myInput-18').val();


    if (p && o) {


      this.firstreview = false;
      document.getElementById('o-err').style.visibility = 'hidden';
      document.getElementById('myInput-17').style.borderColor = 'lightgray';
      document.getElementById('p-err').style.visibility = 'hidden';
      document.getElementById('myInput-18').style.borderColor = 'lightgray';


      event['effective_Date'] = this.effectivedatedata;

      var qwe;
      qwe = Object.assign(event)

      console.log(qwe)
      var qwerty = JSON.stringify(event);
      console.log(qwerty)
      this.saveInLocal(event.position_Id, qwerty);

      this.checkbox_legal = false;
      this.checkbox_bu = false;
      this.positiondata = '';
      this.statusdata = '';
      $('#myInput-1').val('');
      this.costcenterdata = '';
      this.jobcodedata = '';
      this.legaldata = '';
      this.budata = '';
      this.lobdata = '';
      this.verticaldata = '';
      this.subvdata = '';
      this.geozonedata = '';
      this.facilitydata = '';
      this.emptypedata = '';
      this.empclassdata = '';
      this.jobfamilydata = '';
      this.internaldata = '';
      this.variabledata = '';
      this.buhrdata = '';
      this.reportingdata = '';
      var paa =
        document.getElementById('paa') as HTMLInputElement;
      paa.checked = false
      $(".k-button").click();
      $("#drg").css("display", "none");


      $("#myInput-1").attr('disabled', 'disabled');
      $("#myInput-2").attr('disabled', 'disabled');
      $("#myInput-3").attr('disabled', 'disabled');
      $("#myInput-4").attr('disabled', 'disabled');
      $("#myInput-5").attr('disabled', 'disabled');
      $("#myInput-6").attr('disabled', 'disabled');
      $("#myInput-7").attr('disabled', 'disabled');
      $("#myInput-8").attr('disabled', 'disabled');
      $("#myInput-9").attr('disabled', 'disabled');
      $("#myInput-10").attr('disabled', 'disabled');
      $("#myInput-11").attr('disabled', 'disabled');
      $("#myInput-12").attr('disabled', 'disabled');
      $("#myInput-13").attr('disabled', 'disabled');
      $("#myInput-14").attr('disabled', 'disabled');
      $("#myInput-15").attr('disabled', 'disabled');
      $("#myInput-16").attr('disabled', 'disabled');
      $("#myInput-17").attr('disabled', 'disabled');
      $("#myInput-18").attr('disabled', 'disabled');




      for (var i = 0; i < this.positioncodejsonsave.Output.length; i++)
        if (this.positioncodejsonsave.Output[i] == event.position_Id) {
          this.positioncodejsonsave.Output.splice(i, 1);
          console.log('ashish')
        }

      this.position();
      $(".k-file").css("display", "none");

    }

    else {
      document.getElementById('o-err').style.visibility = 'visible';
      document.getElementById('myInput-17').style.borderColor = 'red';
      document.getElementById('p-err').style.visibility = 'visible';
      document.getElementById('myInput-18').style.borderColor = 'red';


    }
    if (o) {
      document.getElementById('o-err').style.visibility = 'hidden';
      document.getElementById('myInput-17').style.borderColor = 'lightgray';
    }
    if (p) {
      document.getElementById('p-err').style.visibility = 'hidden';
      document.getElementById('myInput-18').style.borderColor = 'lightgray';
    }


  }


  windowsreload() {
    window.location.reload();
  }


  hidedd0() {

    var i

    var l = $('#myInput').val();

    var count = Object.keys(this.positioncodejsonsave.Output).length;
    console.log("length is " + count);


    var flag = 0;
    for (i = 0; i < count; i++) {

      if (this.positioncodejsonsave.Output[i] != l) {
        console.log("divaker !=")
        flag = 1
        continue
      }
      else {
        console.log("divaker ==")
        flag = 0
        break
      }
    }
    if (flag == 1) {
      $('#myInput').val(this.positiondata);
      console.log("qwerty")
    }


    document.getElementById("myDropdown").classList.remove("show");
    console.warn('Ok Tested ! ');
  }

  showdd0() {
    document.getElementById("myDropdown").classList.add("show");
    console.warn('Ok Tested ! ');
  }

  hidedd2() {

    var i

    var l = $('#myInput-2').val();

    var count = Object.keys(this.statusjson.Output).length;
    console.log("length is " + count);


    var flag = 0;
    for (i = 0; i < count; i++) {
      console.log("asdcathadbfxc")
      if (this.statusjson.Output[i] != l) {
        console.log("divaker !=")
        flag = 1
        continue
      }
      else {
        console.log("divaker ==")
        flag = 0
        break
      }
    }
    if (flag == 1) {
      $('#myInput-2').val(this.statusdata);
      console.log("qwerty")
    }

    document.getElementById("myDropdown-2").classList.remove("show");
    console.warn('Ok Tested ! ');
  }

  showdd2() {
    document.getElementById("myDropdown-2").classList.add("show");
    console.warn('Ok Tested ! ');
  }

  // hidedd3() {showdd4

  // showdd3() {


  //   document.getElementById("myDropdown-3").classList.add("show");
  //   console.warn('Ok Tested ! ');
  // }

  // hidedd4() {

  //   var i

  //   var l = $('#myInput-4').val();

  //   var count = Object.keys(this.bussu.Output).length;
  //   console.log("length is " + count);


  //   var flag = 0;
  //   for (i = 0; i < count; i++) {
  //     console.log("asdcathadbfxc")
  //     if (this.bussu.Output[i] != l) {
  //       console.log("divaker !=")
  //       flag = 1
  //       continue
  //     }
  //     else {
  //       console.log("divaker ==")
  //       flag = 0
  //       break
  //     }
  //   }
  //   if (flag == 1) {
  //     $('#myInput-4').val(this.budata);
  //     console.log("qwerty")
  //   }
  //   document.getElementById("myDropdown-4").classList.remove("show");
  //   console.warn('Ok Tested ! ');
  // }

  // showdd4() {
  //   document.getElementById("myDropdown-4").classList.add("show");
  //   console.warn('Ok Tested ! ');
  // }

  // hidedd5() {

  //   var i

  //   var l = $('#myInput-5').val();

  //   var count = Object.keys(this.lobsegment.Output).length;
  //   console.log("length is " + count);


  //   var flag = 0;
  //   for (i = 0; i < count; i++) {

  //     if (this.lobsegment.Output[i] != l) {
  //       console.log("divaker !=")
  //       flag = 1
  //       continue
  //     }
  //     else {
  //       console.log("divaker ==")
  //       flag = 0
  //       break
  //     }
  //   }
  //   if (flag == 1) {
  //     $('#myInput-5').val(this.lobdata);
  //     console.log("qwerty")
  //   }


  //   document.getElementById("myDropdown-5").classList.remove("show");
  //   console.warn('Ok Tested ! ');
  // }

  // showdd5() {
  //   document.getElementById("myDropdown-5").classList.add("show");
  //   console.warn('Ok Tested ! ');
  // }

  // hidedd6() {

  //   var i

  //   var l = $('#myInput-6').val();

  //   var count = Object.keys(this.vertical.Output).length;
  //   console.log("length is " + count);


  //   var flag = 0;
  //   for (i = 0; i < count; i++) {
  //     console.log("asdcathadbfxc")
  //     if (this.vertical.Output[i] != l) {
  //       console.log("divaker !=")
  //       flag = 1
  //       continue
  //     }
  //     else {
  //       console.log("divaker ==")
  //       flag = 0
  //       break
  //     }
  //   }
  //   if (flag == 1) {
  //     $('#myInput-6').val(this.verticaldata);
  //     console.log("qwerty")
  //   }


  //   document.getElementById("myDropdown-6").classList.remove("show");
  //   console.warn('Ok Tested ! ');
  // }


  // hidedd7() {

  //   var i

  //   var l = $('#myInput-7').val();

  //   var count = Object.keys(this.subvertical.Output).length;
  //   console.log("length is " + count);


  //   var flag = 0;
  //   for (i = 0; i < count; i++) {
  //     console.log("asdcathadbfxc")
  //     if (this.subvertical.Output[i] != l) {
  //       console.log("divaker !=")
  //       flag = 1
  //       continue
  //     }
  //     else {
  //       console.log("divaker ==")
  //       flag = 0
  //       break
  //     }
  //   }
  //   if (flag == 1) {
  //     $('#myInput-7').val(this.subvdata);
  //     console.log("qwerty")
  //   }


  //   if (l == '') {
  //     $('#myInput-7').val('');
  //     console.log("qwerty")
  //   }



  //   document.getElementById("myDropdown-7").classList.remove("show");
  //   console.warn('Ok Tested ! ');
  // }

  // showdd7() {
  //   document.getElementById("myDropdown-7").classList.add("show");
  //   console.warn('Ok Tested ! ');
  // }

  // hidedd8() {

  //   var i

  //   var l = $('#myInput-8').val();

  //   var count = Object.keys(this.costcenter.Output).length;
  //   console.log("length is " + count);


  //   var flag = 0;
  //   for (i = 0; i < count; i++) {
  //     console.log("asdcathadbfxc")
  //     if (this.costcenter.Output[i] != l) {
  //       console.log("divaker !=")
  //       flag = 1
  //       continue
  //     }
  //     else {
  //       console.log("divaker ==")
  //       flag = 0
  //       break
  //     }
  //   }
  //   if (flag == 1) {
  //     $('#myInput-8').val(this.costcenterdata);
  //     console.log("qwerty")
  //   }



  //   document.getElementById("myDropdown-8").classList.remove("show");
  //   console.warn('Ok Tested ! ');
  // }

  // showdd8() {
  //   document.getElementById("myDropdown-8").classList.add("show");
  //   console.warn('Ok Tested ! ');
  // }

  hidedd9() {

    var i

    var l = $('#myInput-9').val();

    var count = Object.keys(this.geozone.Output).length;
    console.log("length is " + count);


    var flag = 0;
    for (i = 0; i < count; i++) {
      console.log("asdcathadbfxc")
      if (this.geozone.Output[i] != l) {
        console.log("divaker !=")
        flag = 1
        continue
      }
      else {
        console.log("divaker ==")
        flag = 0
        break
      }
    }
    if (flag == 1) {
      $('#myInput-9').val(this.geozonedata);
      console.log("qwerty")
    }


    document.getElementById("myDropdown-9").classList.remove("show");
    console.warn('Ok Tested ! ');
  }

  showdd9() {
    document.getElementById("myDropdown-9").classList.add("show");
    console.warn('Ok Tested ! ');
  }

  hidedd10() {

    var i

    var l = $('#myInput-10').val();

    var count = Object.keys(this.facility.Output).length;
    console.log("length is " + count);


    var flag = 0;
    for (i = 0; i < count; i++) {
      console.log("asdcathadbfxc")
      if (this.facility.Output[i] != l) {
        console.log("divaker !=")
        flag = 1
        continue
      }
      else {
        console.log("divaker ==")
        flag = 0
        break
      }
    }
    if (flag == 1) {
      $('#myInput-10').val(this.facilitydata);
      console.log("qwerty")
    }


    document.getElementById("myDropdown-10").classList.toggle("show");
    console.warn('Ok Tested ! ');
  }

  showdd10() {
    document.getElementById("myDropdown-10").classList.add("show");
    console.warn('Ok Tested ! ');
  }

  hidedd11() {

    var i

    var l = $('#myInput-11').val();

    var count = Object.keys(this.employeeclass.Output).length;
    console.log("length is " + count);


    var flag = 0;
    for (i = 0; i < count; i++) {
      console.log("asdcathadbfxc")
      if (this.employeeclass.Output[i] != l) {
        console.log("divaker !=")
        flag = 1
        continue
      }
      else {
        console.log("divaker ==")
        flag = 0
        break
      }
    }
    if (flag == 1) {
      $('#myInput-11').val(this.empclassdata);
      console.log("qwerty")
    }


    document.getElementById("myDropdown-11").classList.toggle("show");
    console.warn('Ok Tested ! ');
  }

  showdd11() {
    document.getElementById("myDropdown-11").classList.add("show");
    console.warn('Ok Tested ! ');
  }

  hidedd12() {

    var i

    var l = $('#myInput-12').val();

    var count = Object.keys(this.employeetype.Output).length;
    console.log("length is " + count);


    var flag = 0;
    for (i = 0; i < count; i++) {
      console.log("asdcathadbfxc")
      if (this.employeetype.Output[i] != l) {
        console.log("divaker !=")
        flag = 1
        continue
      }
      else {
        console.log("divaker ==")
        flag = 0
        break
      }
    }
    if (flag == 1) {
      $('#myInput-12').val(this.emptypedata);
      console.log("qwerty")
    }


    document.getElementById("myDropdown-12").classList.toggle("show");
    console.warn('Ok Tested ! ');
  }

  showdd12() {
    document.getElementById("myDropdown-12").classList.add("show");
    console.warn('Ok Tested ! ');
  }

  hidedd13() {

    var i

    var l = $('#myInput-13').val();

    var count = Object.keys(this.jobfamily.Output).length;
    console.log("length is " + count);


    var flag = 0;
    for (i = 0; i < count; i++) {
      console.log("asdcathadbfxc")
      if (this.jobfamily.Output[i] != l) {
        console.log("divaker !=")
        flag = 1
        continue
      }
      else {
        console.log("divaker ==")
        flag = 0
        break
      }
    }
    if (flag == 1) {
      $('#myInput-13').val(this.jobfamilydata);
      console.log("qwerty")
    }


    document.getElementById("myDropdown-13").classList.toggle("show");
    console.warn('Ok Tested ! ');
  }

  showdd13() {
    document.getElementById("myDropdown-13").classList.add("show");
    console.warn('Ok Tested ! ');
  }

  hidedd14() {

    var i

    var l = $('#myInput-14').val();

    var count = Object.keys(this.jobcode.Output).length;
    console.log("length is " + count);


    var flag = 0;
    for (i = 0; i < count; i++) {
      console.log("asdcathadbfxc")
      if (this.jobcode.Output[i] != l) {
        console.log("divaker !=")
        flag = 1
        continue
      }
      else {
        console.log("divaker ==")
        flag = 0
        break
      }
    }
    if (flag == 1) {
      $('#myInput-14').val(this.jobcodedata);
      console.log("qwerty")
    }


    document.getElementById("myDropdown-14").classList.toggle("show");
    console.warn('Ok Tested ! ');
  }

  showdd14() {
    document.getElementById("myDropdown-14").classList.add("show");
    console.warn('Ok Tested ! ');
  }

  hidedd15() {

    var i

    var l = $('#myInput-15').val();

    var count = Object.keys(this.internalspecilization.Output).length;
    console.log("length is " + count);


    var flag = 0;
    for (i = 0; i < count; i++) {
      console.log("asdcathadbfxc")
      if (this.internalspecilization.Output[i] != l) {
        console.log("divaker !=")
        flag = 1
        continue
      }
      else {
        console.log("divaker ==")
        flag = 0
        break
      }
    }
    if (flag == 1) {
      $('#myInput-15').val(this.internaldata);
      console.log("qwerty")
    }

    if (l == '') {
      $('#myInput-15').val('');
      console.log("qwerty")
    }


    document.getElementById("myDropdown-15").classList.toggle("show");
    console.warn('Ok Tested ! ');
  }

  showdd15() {
    document.getElementById("myDropdown-15").classList.add("show");
    console.warn('Ok Tested ! ');
  }

  hidedd16() {

    var i

    var l = $('#myInput-16').val();

    var count = Object.keys(this.variablepay.Output).length;
    console.log("length is " + count);


    var flag = 0;
    for (i = 0; i < count; i++) {
      console.log("asdcathadbfxc")
      if (this.variablepay.Output[i] != l) {
        console.log("divaker !=")
        flag = 1
        continue
      }
      else {
        console.log("divaker ==")
        flag = 0
        break
      }
    }
    if (flag == 1) {
      $('#myInput-16').val(this.variabledata);
      console.log("qwerty")
    }


    document.getElementById("myDropdown-16").classList.toggle("show");
    console.warn('Ok Tested ! ');
  }

  showdd16() {
    document.getElementById("myDropdown-16").classList.add("show");
    console.warn('Ok Tested ! ');
  }

  hidedd17() {

    var i

    var l = $('#myInput-17').val();

    var count = Object.keys(this.reportingmanager.Output).length;
    console.log("length is " + count);


    var flag = 0;
    for (i = 0; i < count; i++) {
      console.log("asdcathadbfxc")
      if (this.reportingmanager.Output[i] != l) {
        console.log("divaker !=")
        flag = 1
        continue
      }
      else {
        console.log("divaker ==")
        flag = 0
        break
      }
    }
    if (flag == 1) {
      $('#myInput-17').val(this.reportingdata);
      console.log("qwerty")
    }


    document.getElementById("myDropdown-17").classList.toggle("show");
    console.warn('Ok Tested ! ');
  }

  showdd17() {
    document.getElementById("myDropdown-17").classList.add("show");
    console.warn('Ok Tested ! ');
  }

  hidedd18() {

    var i

    var l = $('#myInput-18').val();

    var count = Object.keys(this.buhrspoc.Output).length;
    console.log("length is " + count);


    var flag = 0;
    for (i = 0; i < count; i++) {
      console.log("asdcathadbfxc")
      if (this.buhrspoc.Output[i] != l) {
        console.log("divaker !=")
        flag = 1
        continue
      }
      else {
        console.log("divaker ==")
        flag = 0
        break
      }
    }
    if (flag == 1) {
      $('#myInput-18').val(this.buhrdata);
      console.log("qwerty")
    }


    document.getElementById("myDropdown-18").classList.toggle("show");
    console.warn('Ok Tested ! ');
  }

  showdd18() {
    document.getElementById("myDropdown-18").classList.add("show");
    console.warn('Ok Tested ! ');
  }


  saveInLocal(key, val): void {


    console.log('recieved= key:' + key + 'value:' + val);
    sessionStorage.setItem(key, val);
    console.log('done');
    console.warn(sessionStorage.length)


  }

  checkmax() {
    if (sessionStorage.length < 5) {

    }
    else {

      this.maxform = true;

    }
  }

  getfromlocal(event) {


    event['effective_Date'] = $('#myInput-1').val();

    var qwe;
    qwe = Object.assign(event)

    console.log(qwe)
    var qwerty = JSON.stringify(event);
    console.log(qwerty)
    this.saveInLocal(event.position_Id, qwerty);
    var json = {
    }

    for (let i = 0; i < sessionStorage.length - 1; i++) {
      this.datasession.key[i] = sessionStorage.key(i);
      let key = sessionStorage.key(i);
      this.value = sessionStorage.getItem(key);
      this.datasession.value[i] = JSON.parse(this.value);
    }
    var qwe

    var alldata;
    alldata = [this.datasession.value];
    json = this.datasession.value

    console.log(this.datasession);

    this.httpClient
      .post('http://13.90.80.185:3337/user/update_api/', json)
      .map((response: any) => response)
      .do(data => qwe = data)
      .subscribe(() => {
        console.log(json, qwe);
        sessionStorage.clear();
        $("#s-btn").click()

      });


  }


  employeecodesend(event) {

    if ($("#z").val()) {
      $("#loader1").css("visibility", "visible");
      console.log(event)
      $("#z").css("borderColor", "lightgray");
      $("#z-err").css("visibility", "hidden");
      this.edmService.httpPost('/user/login', event).then((s: any) => {
        this.employeecodejsonsave = s;
        console.log(this.employeecodejsonsave)
      })
    }
    else {
      $("#z").css("borderColor", "red");
      $("#z-err").css("visibility", "visible");
      $("#zz").val("")
    }

  }


  printemail() {
    console.log(this.employeecodejsonsave)
    $('#zz').val(this.employeecodejsonsave.Employee_information.Employee_email)

    if ($('#zz').val()) {
      $("#myInput").removeAttr("disabled");
      this.cookie_popup();
    }
    else {
      console.log("Hello")
    }
    this.httpClient
      .post('http://13.90.80.185:3337/user/json/', this.qwerty)
      .map((response: any) => response)
      .do(data => this.func(data))
      .subscribe(() =>
        console.log(this.positioncodejsonsave),
        $("#loader1").css("visibility", "hidden")
      );


  }
  valid_a() {
    if ($("#myInput").val()) {
      $("#myInput").css("borderColor", "lightgray");
      $("#a-err").css("visibility", "hidden");
    }
    else {
      $("#myInput").css("borderColor", "red");
      $("#a-err").css("visibility", "visible");
    }

  }

  DateSelect(event) {

    var qwe = JSON.stringify(event)
    console.log(qwe)
    var str = "";
    str += event['year']
    str += '-'
    if (event['month'].length == 1) {
      str += "0"
      str += event['month']
    }
    else {
      str += event['month']
    }
    str += '-'
    str += event['day']

    console.log(str)

    this.effectivedatedata = str

  }
  save() {
    var popup = document.getElementById("exampleModal");
    popup.classList.remove("in");
    console.log("You pressed OK!");
    var code = $('#z').val();
    this.cookieService.set('employ_code', code);
    this.cookieValue = this.cookieService.get('employ_code');
    $('#z').val(this.cookieValue);
    console.log(this.cookieValue);
  }
  cancel() {
    var popup = document.getElementById("exampleModal");
    popup.classList.remove("in");
    console.log("You pressed Cancel!");
    this.cookieService.delete('employ_code');
  }
  cookie_popup() {
    var popup = document.getElementById("exampleModal");
    popup.classList.add("in");


  }


  onChange(event): void {

    console.log(event.timeStamp)
    this.effectivedatedata = event.timeStamp;

  }


  toreviewform(event) {
    var qwe: boolean = false;


    var o = $('#myInput-17').val();
    var p = $('#myInput-18').val();


    if (p && o) {

      document.getElementById('o-err').style.visibility = 'hidden';
      document.getElementById('myInput-17').style.borderColor = 'lightgray';
      document.getElementById('p-err').style.visibility = 'hidden';
      document.getElementById('myInput-18').style.borderColor = 'lightgray';

      event['effective_Date'] = $('#myInput-1').val();




      console.log(event)
      var qwerty = JSON.stringify(event);
      console.log(qwerty)

      for (let i = 0; i < sessionStorage.length - 1; i++) {

        this.datasessiontable.key[i] = sessionStorage.key(i);
        if (this.datasessiontable.key[i] == event.position_Id) {
          qwe = true;

        }
        else {
          qwe = false;
        }

      }

      if (qwe == false) {
        this.saveInLocal(event.position_Id, qwerty);
      }
      else {

        sessionStorage.removeItem(event.position_Id)
        this.saveInLocal(event.position_Id, qwerty);

      }



      for (let i = 0; i < sessionStorage.length - 1; i++) {
        this.datasession.key[i] = sessionStorage.key(i);
        let key = sessionStorage.key(i);
        this.value = sessionStorage.getItem(key);

        this.datasession.value[i] = JSON.parse(this.value);
      }
      console.log('length is', sessionStorage.length)

      $(".k-button").click();

      document.getElementById('position').style.display = 'none';
      document.getElementById('formdatadiv').style.display = 'block';

    }

    else {
      document.getElementById('o-err').style.visibility = 'visible';
      document.getElementById('myInput-17').style.borderColor = 'red';
      document.getElementById('p-err').style.visibility = 'visible';
      document.getElementById('myInput-18').style.borderColor = 'red';


    }
    if (o) {
      document.getElementById('o-err').style.visibility = 'hidden';
      document.getElementById('myInput-17').style.borderColor = 'lightgray';
    }
    if (p) {
      document.getElementById('p-err').style.visibility = 'hidden';
      document.getElementById('myInput-18').style.borderColor = 'lightgray';
    }


  }


  firstreviewform() {
    for (let i = 0; i < sessionStorage.length - 1; i++) {
      this.datasession.key[i] = sessionStorage.key(i);
      let key = sessionStorage.key(i);
      this.value = sessionStorage.getItem(key);

      this.datasession.value[i] = JSON.parse(this.value);
    }
    document.getElementById('position').style.display = 'none';
    document.getElementById('formdatadiv').style.display = 'block';
  }


  takeme() {
    document.getElementById('position').style.display = 'block';
    document.getElementById('bulk').style.display = 'none';
    document.getElementById('formdatadiv').style.display = 'none';
    var bulk_css = document.getElementById("a2");
    bulk_css.classList.remove("bulk-css");
    var reclass_css = document.getElementById("a1");
    reclass_css.classList.remove("reclass-css");

  }





  selectchange(event: any) {

    console.log(event)
    this.deletedata = (event.target.value)
    console.log(this.deletedata)

  }



  deletedataintable(index) {
    sessionStorage.removeItem(this.deletedata)
    var ttt = $('.' + this.deletedata)
    ttt.remove();



  }

  edittabledata() {
    this.editvaluetable = sessionStorage.getItem(this.deletedata);
    this.editvaluetable = JSON.parse(this.editvaluetable)
    console.log(this.editvaluetable.status)

    this.effectivedatedata = this.editvaluetable.effective_Date.substr(0, 10)
    this.positiondata = this.editvaluetable.position_Id;
    this.statusdata = this.editvaluetable.status;
    this.costcenterdata = this.editvaluetable.cost_Center;
    this.jobcodedata = this.editvaluetable.job_Code;
    this.legaldata = this.editvaluetable.legal_Entity;
    this.budata = this.editvaluetable.bu_Ssu;
    this.lobdata = this.editvaluetable.lob_Segment;
    this.verticaldata = this.editvaluetable.vertical;
    this.subvdata = this.editvaluetable.sub_Vertical;
    this.geozonedata = this.editvaluetable.geozone;
    this.facilitydata = this.editvaluetable.facility;
    this.emptypedata = this.editvaluetable.employee_Type;
    this.empclassdata = this.editvaluetable.employee_Class;
    this.jobfamilydata = this.editvaluetable.job_Family;
    this.internaldata = this.editvaluetable.internal_Specialization;
    this.variabledata = this.editvaluetable.variable_Pay_Plan_Type;
    this.buhrdata = this.editvaluetable.bu_Hr_Spoc;
    this.reportingdata = this.editvaluetable.reporting_Manager;
    this.position();
  }


  check(value) {
    console.log(value);

    if (value == 1) {
      for (let i = 1; i < sessionStorage.length - 1; i++) {
        this.datasession.key[i] = sessionStorage.key(i - 1);
        let key = sessionStorage.key(i);
        this.value = sessionStorage.getItem(key);

        this.datasession.value[i] = JSON.parse(this.value);
        console.log(i);
      }

      var options = {
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalseparator: '.',
        showLabels: true,
        showTitle: true,
        useBom: true,
        noDownload: true,
        headers: [
          "Position ID", "Status", "Effective Date", "Legal Entity", "Bu/SSU", "Lob Segment", "Vertical", "Sub  Vertical", "Cost center", "Geozone", "Facility", "Employee Class", "Employee Type", "Job Family", "Job Code", "Internal Specialization", "Variable Pay Plan Type", "Reporting Manager", "Bu Hr SPOC"
        ]
      };

      console.log('234')
      new Angular5Csv(this.datasession.value, 'My Rep', options);
      console.log(options)
    }
    if (value == 2) {

    }
  }



  checkmax1() {
    if (sessionStorage.length < 6) {
      this.maxform = false

    }
    else {

      this.maxform = true;

    }
  }

  activatesection(id) {
    this.currentsection = id;
  }

  /* AKASH MADE CODE CHANGES */
  myFunction(t) {

    // if (this.positioncodejson.Output.length > 0) {
    //   this.showPositionData = true;
    // }
    // if (t == 'hide') {
    //   this.showPositionData = false;
    // }
    this.myFunction3(t);
  }

  myFunction3(id) {
    $("#" + id).toggle("fast");
  }
  showStatusDropDown(t) {
    if (this.showActivePositionData == false) {
      this.showActivePositionData = true;
    }
    if (t == 'hide') {
      this.showActivePositionData = false;
    }
  }

  filterFunction3() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("myInput-3");
    filter = input.value.toUpperCase();
    let div = document.getElementById("myDropdown-3");
    a = div.getElementsByTagName("a");
    for (i = 0; i < a.length; i++) {
      if (a[i].innerHTML.toUpperCase().indexOf(filter) > -1) {
        a[i].style.display = "";
      } else {
        a[i].style.display = "none";
      }
    }
  }
}
